from common.json import ModelEncoder
from .models import Fandom, Account, Profile, Post, Comment


################# ACCOUNTS ################


class FandomEncoder(ModelEncoder):
    model = Fandom
    properties = [
        "id",
        "eng_title",
        "romaji_title",
        "year",
    ]



class AccountEncoder(ModelEncoder):
    model = Account
    properties = [
        "id",
        "username",
        "password",
        "email",
    ]



class ProfileEncoder(ModelEncoder):
    model = Profile
    properties = [
        "id",
        "first_name",
        "last_name",
        "canon",
        "fandom",
        "account",
    ]
    encoders = {
        "fandom": FandomEncoder(),
        "account": AccountEncoder(),
    }



################# COMMENTS AND POSTS ################



class PostEncoder(ModelEncoder):
    model = Post
    properties = [
        "id",
        "profile",
        "image",
        "message",
    ]
    encoders = {
        "profile": ProfileEncoder(),
    }



class CommentEncoder(ModelEncoder):
    model = Comment
    properties = [
        "id",
        "profile",
        "post",
        "message",
        "image",
    ]
    encoders = {
        "post": PostEncoder(),
        "profile": ProfileEncoder(),
    }