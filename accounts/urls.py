from django.urls import path
from .views import (
    fandom_detail,
    fandom_list,
    account_detail,
    account_list,
    profile_detail,
    profile_list,
    post_detail,
    post_list,
    comment_detail,
    comment_list
)


urlpatterns = [
    
    path("fandom/", fandom_list, name="fandom_list"),
    path("fandom/<int:id>/", fandom_detail, name="fandom_detail"),

    path("account/", account_list, name="account_list"),
    path("account/<int:id>/", account_detail, name="account_detail"),

    path("profile/", profile_list, name="profile_list"),
    path("profile/<int:id>/", profile_detail, name="profile_detail"),

    path("post/", post_list, name="post_list"),
    path("post/<int:id>/", post_detail, name="post_detail"),

    path("comment/", comment_list, name="comment_list"),
    path("comment/<int:id>/", comment_detail, name="comment_detail"),

]