from django.contrib import admin
from .models import Fandom, Account, Profile, Post, Comment

# Register your models here.
@admin.register(Fandom)
class FandomAdmin(admin.ModelAdmin):
    list_display = [
        "eng_title",
        "romaji_title",
        "year"
    ]

@admin.register(Account)
class AccountAdmin(admin.ModelAdmin):
    list_display = [
        "username",
        "password",
        "email"
    ]

@admin.register(Profile)
class ProfileAdmin(admin.ModelAdmin):
    list_display = [
        "first_name",
        "last_name",
        "canon",
        "fandom",
        "account"
    ]
    
@admin.register(Post)
class PostAdmin(admin.ModelAdmin):
    list_display = [
        "profile",
        "image",
        "message"
    ]

@admin.register(Comment)
class CommentAdmin(admin.ModelAdmin):
    list_display = [
        "profile",
        "post",
        "message",
        "image"
    ]