from django.shortcuts import render
from django.views.decorators.http import require_http_methods
from django.http import JsonResponse
import json

from .models import (
    Fandom,
    Account,
    Profile,
    Post,
    Comment,
)

from .encoders import (
    FandomEncoder,
    AccountEncoder,
    ProfileEncoder,
    PostEncoder,
    CommentEncoder,
)




################################# FANDOM LIST AND DETAILS #################################




@require_http_methods(["GET", "POST"])
def fandom_list(request):

    if request.method == "GET":
        fandoms = Fandom.objects.all()
        return JsonResponse(
            {"fandoms": fandoms},
            encoder = FandomEncoder
        )
    
    else: #POST
        content = json.loads(request.body)
        fandom = Fandom.objects.create(**content)
        return JsonResponse(
            fandom,
            encoder = FandomEncoder,
            safe=False,
        )
    

@require_http_methods(["GET", "DELETE", "PUT"])
def fandom_detail(request, id):
    
    if request.method == "GET":
        try:
            fandom = Fandom.objects.get(id=id)
            return JsonResponse(
                fandom,
                encoder=FandomEncoder,
                safe=False
            )
        except Fandom.DoesNotExist:
            response = JsonResponse({"message": "Fandom does not exist!"})
            response.status_code = 404
            return response
        
    elif request.method == "DELETE":
        try:
            fandom = Fandom.objects.get(id=id)
            fandom.delete()
            return JsonResponse(
                fandom,
                encoder=FandomEncoder,
                safe=False,
            )
        except Fandom.DoesNotExist:
            response = JsonResponse({"message": "Fandom does not exist!"})
            response.status_code = 404
            return response
        
    else: #PUT
        try:
            content = json.loads(request.body)
            Fandom.objects.filter(id=id).update(**content)
            fandom = Fandom.objects.get(id=id)
            return JsonResponse(
                fandom,
                encoder=FandomEncoder,
                safe=False,
            )
        except Fandom.DoesNotExist:
            response = JsonResponse({"message": "Fandom does not exist!"})
            response.status_code = 404
            return response
        



################################# ACCOUNT LIST AND DETAILS #################################




@require_http_methods(["GET", "POST"])
def account_list(request):

    if request.method == "GET":
        accounts = Account.objects.all()
        return JsonResponse(
            {"accounts": accounts},
            encoder = AccountEncoder
        )
    
    else: #POST
        content = json.loads(request.body)
        account = Account.objects.create(**content)
        return JsonResponse(
            account,
            encoder = AccountEncoder,
            safe=False,
        )
    

@require_http_methods(["GET", "DELETE", "PUT"])
def account_detail(request, id):
    
    if request.method == "GET":
        try:
            account = Account.objects.get(id=id)
            return JsonResponse(
                account,
                encoder=AccountEncoder,
                safe=False
            )
        except Account.DoesNotExist:
            response = JsonResponse({"message": "Acct does not exist!"})
            response.status_code = 404
            return response
        
    elif request.method == "DELETE":
        try:
            account = Account.objects.get(id=id)
            account.delete()
            return JsonResponse(
                account,
                encoder=AccountEncoder,
                safe=False,
            )
        except Account.DoesNotExist:
            response = JsonResponse({"message": "Acct does not exist!"})
            response.status_code = 404
            return response
        
    else: #PUT
        try:
            content = json.loads(request.body)
            Account.objects.filter(id=id).update(**content)
            account = Account.objects.get(id=id)
            return JsonResponse(
                account,
                encoder=AccountEncoder,
                safe=False,
            )
        except Account.DoesNotExist:
            response = JsonResponse({"message": "Acct does not exist!"})
            response.status_code = 404
            return response
        



################################# PROFILE LIST AND DETAILS #################################




@require_http_methods(["GET", "POST"])
def profile_list(request):

    if request.method == "GET":
        profiles = Profile.objects.all()
        return JsonResponse(
            {"profiles": profiles},
            encoder = ProfileEncoder
        )
    

    else: #POST
        content = json.loads(request.body)

        try:
            fandom_id = content["fandom"]
            fandom = Fandom.objects.get(id=fandom_id)
            content["fandom"] = fandom
        except Fandom.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid Fandom ID"},
                status = 404,
            )
        
        try:
            account_id = content["account"]
            account = Account.objects.get(id=account_id)
            content["account"] = account

        except Account.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid Account ID"},
                status = 404,
            )

        profile = Profile.objects.create(**content)
        return JsonResponse(
            profile,
            encoder = ProfileEncoder,
            safe=False,
        )
    

@require_http_methods(["GET", "DELETE", "PUT"])
def profile_detail(request, id):
    
    if request.method == "GET":
        try:
            profile = Profile.objects.get(id=id)
            return JsonResponse(
                profile,
                encoder=ProfileEncoder,
                safe=False
            )
        except Profile.DoesNotExist:
            response = JsonResponse({"message": "Profile does not exist!"})
            response.status_code = 404
            return response
        
    elif request.method == "DELETE":
        try:
            profile = Profile.objects.get(id=id)
            profile.delete()
            return JsonResponse(
                profile,
                encoder=ProfileEncoder,
                safe=False,
            )
        except Profile.DoesNotExist:
            response = JsonResponse({"message": "Profile does not exist!"})
            response.status_code = 404
            return response
        
    else: #PUT
        try:
            content = json.loads(request.body)
            Profile.objects.filter(id=id).update(**content)
            profile = Profile.objects.get(id=id)
            return JsonResponse(
                profile,
                encoder=ProfileEncoder,
                safe=False,
            )
        except Profile.DoesNotExist:
            response = JsonResponse({"message": "Profile does not exist!"})
            response.status_code = 404
            return response
        



################################# POST LIST AND DETAILS #################################




@require_http_methods(["GET", "POST"])
def post_list(request):

    if request.method == "GET":
        posts = Post.objects.all()
        return JsonResponse(
            {"posts": posts},
            encoder = PostEncoder
        )
    
    else: #POST
        content = json.loads(request.body)
        try:
            profile_id = content["profile"]
            profile = Profile.objects.get(id=profile_id)
            content["profile"] = profile

        except Profile.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid Profile ID"},
                status = 404,
            )

        post = Post.objects.create(**content)
        return JsonResponse(
            post,
            encoder = PostEncoder,
            safe=False,
        )
    

@require_http_methods(["GET", "DELETE", "PUT"])
def post_detail(request, id):
    
    if request.method == "GET":
        try:
            post = Post.objects.get(id=id)
            return JsonResponse(
                post,
                encoder=PostEncoder,
                safe=False
            )
        except Post.DoesNotExist:
            response = JsonResponse({"message": "Post does not exist!"})
            response.status_code = 404
            return response
        
    elif request.method == "DELETE":
        try:
            post = Post.objects.get(id=id)
            post.delete()
            return JsonResponse(
                post,
                encoder=PostEncoder,
                safe=False,
            )
        except Post.DoesNotExist:
            response = JsonResponse({"message": "Post does not exist!"})
            response.status_code = 404
            return response
        
    else: #PUT
        try:
            content = json.loads(request.body)
            Post.objects.filter(id=id).update(**content)
            post = Post.objects.get(id=id)
            return JsonResponse(
                post,
                encoder=PostEncoder,
                safe=False,
            )
        except Post.DoesNotExist:
            response = JsonResponse({"message": "Post does not exist!"})
            response.status_code = 404
            return response
        



################################# COMMENT LIST AND DETAILS #################################




@require_http_methods(["GET", "POST"])
def comment_list(request):

    if request.method == "GET":
        comments = Comment.objects.all()
        return JsonResponse(
            {"comments": comments},
            encoder = CommentEncoder
        )
    
    else: #POST

        content = json.loads(request.body)

        try:
            post_id = content["post"]
            post = Post.objects.get(id=post_id)
            content["post"] = post

        except Post.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid Post ID"},
                status = 404,
            )
        
        
        try:
            profile_id = content["profile"]
            profile = Profile.objects.get(id=profile_id)
            content["profile"] = profile

        except Profile.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid Profile ID"},
                status = 404,
            )
        
        
        comment = Comment.objects.create(**content)
        return JsonResponse(
            comment,
            encoder = CommentEncoder,
            safe=False,
        )
    

@require_http_methods(["GET", "DELETE", "PUT"])
def comment_detail(request, id):
    
    if request.method == "GET":
        try:
            comment = Comment.objects.get(id=id)
            return JsonResponse(
                comment,
                encoder=CommentEncoder,
                safe=False
            )
        except Comment.DoesNotExist:
            response = JsonResponse({"message": "Comment does not exist!"})
            response.status_code = 404
            return response
        
    elif request.method == "DELETE":
        try:
            comment = Comment.objects.get(id=id)
            comment.delete()
            return JsonResponse(
                comment,
                encoder=CommentEncoder,
                safe=False,
            )
        except Comment.DoesNotExist:
            response = JsonResponse({"message": "Comment does not exist!"})
            response.status_code = 404
            return response
        
    else: #PUT
        try:
            content = json.loads(request.body)
            Comment.objects.filter(id=id).update(**content)
            comment = Comment.objects.get(id=id)
            return JsonResponse(
                comment,
                encoder=CommentEncoder,
                safe=False,
            )
        except Comment.DoesNotExist:
            response = JsonResponse({"message": "Comment does not exist!"})
            response.status_code = 404
            return response