from django.db import models

# Create your models here.

class Fandom(models.Model):
    eng_title = models.CharField(max_length=250)
    romaji_title = models.CharField(max_length=250)
    year = models.BigIntegerField()

    def __str__(self):
        return self.eng_title



class Account(models.Model):
    username = models.CharField(max_length=50)
    password = models.CharField(max_length=75)
    email = models.EmailField()

    def __str__(self):
        return self.username


class Profile(models.Model):
    first_name = models.CharField(max_length=100)
    last_name = models.CharField(max_length=100)
    canon = models.BooleanField(default=False)
    fandom = models.ForeignKey(
        Fandom,
        related_name="fandom",
        on_delete=models.PROTECT
    )
    account = models.ForeignKey(
        Account,
        related_name="account",
        on_delete=models.CASCADE
    )

    def __str__(self):
        return self.last_name



class Post(models.Model):
    profile = models.ForeignKey(
        Profile,
        related_name="profile",
        on_delete=models.CASCADE,
        blank=True, null=True
    )
    image = models.URLField(blank=True, null=True)
    message = models.TextField()

    def __str__(self):
        return self.message 



class Comment(models.Model):
    profile = models.ForeignKey(
        Profile,
        related_name="comment_profile",
        on_delete=models.CASCADE,
        blank=True, null=True
    )
    post = models.ForeignKey(
        Post,
        related_name="post",
        on_delete=models.CASCADE
    )
    message = models.TextField()
    image = models.URLField(blank=True, null=True)

    def __str__(self):
        return self.message